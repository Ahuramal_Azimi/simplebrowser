package com.amirmohammadazimi.simplebrowser;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;

/**
 * Created by Amir Mohammad Azimi on 4/11/2017.
 */
public class WebViewClient extends android.webkit.WebViewClient {
    ProgressBar pbar;

    public WebViewClient(ProgressBar pbar){
        this.pbar = pbar;
    }





    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {

        // TODO Auto-generated method stub
        super.onPageStarted(view, url, favicon);
        pbar.setVisibility(View.VISIBLE);
    }

    public void onProgressChanged(WebView view, int progress) {
        pbar.setProgress(progress);
        if (progress == 100) {
            pbar.setVisibility(View.GONE);

        } else {
            pbar.setVisibility(View.VISIBLE);

        }
    }


    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        // TODO Auto-generated method stub
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {

        // TODO Auto-generated method stub

        super.onPageFinished(view, url);
        pbar.setVisibility(View.GONE);
    }

}


